package com.epam.rd.java.basic.task7.db.entity;

import java.util.Objects;

public class User {

	private int id;

	private String login;

	@Override
	public String toString() {
		return login ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;
		var user = (User) o;
		return Objects.equals(login, user.login);
	}

	@Override
	public int hashCode() {
		return Objects.hash(login);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		var newUser = new User();
		newUser.setLogin(login);
		return newUser;
	}

}
